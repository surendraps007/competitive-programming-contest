### Problem Statement 1

All numbers raised to the power of 3 and ending in 888 are known as Three-eightening numbers. Write a program that decides whether a given number is a Three-Eightening number or not.

**Input Format:**

The first line contains the number of testcases T.

Then T lines follow consisting of integers N.


**Output Format:**

For each test-case print Y, if the number is Three-Eightening, or N if the number is not.
  
**Constraints:**

1 <= T <= 1000000

1 <= Y <= 1000000

**Sample Input:**
```
1
192
```
**Sample Output:**
```
Y
```