### Problem Statement 12

An even special integer is one that can be written as a sum of two or more consecutive even numbers. Positive integers that cannot be written as a sum of two or more consecutive even numbers are called non-even special.


**Examples:**


6 is an even special integer because it can be written as 6 = 2 + 4.

12 is also even special because 12 = 2 + 4 + 6

10 is also even special because 10 = 4 + 6


However, 8 is a non-even special integer because it CANNOT be written as a sum of two or more consecutive even numbers.


Find out the total number of even special integers given a set of positive integers.


**Input format**

The first line of input consists of an integer T. Then T lines follow with the input format as follows:


K x<sub>1</sub> x<sub>2</sub> � x<sub>K</sub>


**Output format**

For each test case, output an integer N which is the number of even special integers in the set.

**Constraints**

1 <= T <= 10000

1 <= K <= 100000000000

2 <=x<sub>i</sub> <= 2<sup>1000</sup>


**Sample input**
```
1
5 7 8 12 17 16 4
```

**Sample output**
```
1
```