### Problem Statement 22

The simplest form of error checking code in digital transmission is the parity bit. It’s a single bit added to a piece of binary code. If the number of 1s in the binary code is odd, the parity bit is 1; 0 otherwise.


If an odd number of bits are transmitted incorrectly, the parity bit will be incorrect thus indicating an error in the transmission. 


**Example:**


Suppose Alice wants to send this 7 bit message to Bob:


1100101

She adds the parity bit, marked in bold (and set apart for illustrative purposes), to the start of the message: 
**0** 1100101. (Because there are an even number of 1s in the binary code.)


If Bob receives this message, but the last bit got flipped in the transmission (indicated in bold and set apart for illustrative purposes):


0110010 **0**, he finds that there is a parity error, and hence there’s an error in transmission!


So given an 8 bit binary code, with the first bit being a parity bit, output Y if there is a parity error and N otherwise.


**Input Format:**

The first line of input consists of an integer T. This represents the number of test cases.

T lines follow each containing an 8 bit binary code (the first bit of which is the parity bit).


**Output Format:**

For each test case output Y if there is a parity error, and N otherwise.


**Constraints**


1 <= T <=100000


**Sample Input**
```
2
11111111
01100100
```
**Sample Output**
```
N
Y
```
**Note: When generating test cases, generate a huge number of them 10^10 or so.**
