### Problem Statement 4

Sheila was standing in a maze that resembled a matrix of N * M tiles. Each tile has a number of keys on them, ranging from 1 to 1000. 

She starts at the top left corner of the maze at cell (1,1) [Numbered in the usual matrix format.] She has to reach the bottom left corner, the cell (N,M). At that cell, there is a door leading out of the maze. The door is a magic door that can only be opened if she picks up the maximum number of keys possible. 

However, there is a constraint involved: when she is standing on a given tile, she can only move to the tile immediately below her, or to the right.

Help her get out of the maze!


**Input Format:**

The first line consists of the number of test cases T.

The first line for each test case consists of the two numbers N and M.

The next N lines in each test case consists of M space separated numbers.


**Output Format:**

For each test case, print in a new line the numbers of keys required to open the door.


**Constraints:**

1 <= T <= 10

1 <= N , M <= 1000


**Sample Input:**
```
1
3 3
1 2 4
4 1 8
7 1 6
```
**Sample Output:**
```
21
```