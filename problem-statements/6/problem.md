### Problem Statement 6
Given an integer A, and n, define  p<sup>n</sup> (A) = A ^ (A ^ (A ^(... ^ A)...)) where you have n copies of A.                      

**Example 1:**

p<sup>5</sup>(3) = 3^ (3 ^ (3 ^ (3 ^ 3)))

**Example 2:**

p<sup>8</sup>(6) = 6 ^ (6 ^ (6 ^ (6 ^ (6 ^ (6 ^ (6 ^ 6))))))

**Example 3:**

p<sup>1</sup>(12) = 12

Now, given an integer n, find the last n digits of 3 p<sup>2017</sup> 3.

**Constraints**

1 <= T <= 100

1 <= n <=10000

**Input Format:**

The first line of input consists of the integer T. Then T lines follow each with an integer n.

**Output Format:**

For each test case, print out the last n digits of 3 p<sup>2017</sup> 3.

**Sample Input:**
```
5
3
7
11
12
1
```
**Sample Output:**
```
387
4195387
62464195387
262464195387
7
```