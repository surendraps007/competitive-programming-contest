### Problem Statement 9

You are given a co-ordinate plane with X and Y axes orthogonal to each other. You are also given N where N is a positive integer. 

All points (x,y) such that 0 <=x <= N, 0 <=y <= N, x and y are both integers, are colored one of three colors Red, Green and Blue. Find the coordinates of a pair of points of the same color such that the distance between these two points is the maximum possible.

Suppose there are multiple pairs of points of the same color whose distance between them are maximal. 

Then report the pair of points such that the x co-ordinate of the midpoint of the line joining the pair is minimum. If again there are multiple pairs of points that satisfy this criterion, then report the pair such that the y co-ordinate of the midpoint of the line joining the pair is minimum.

If (yet again!) there are multiple pairs of points that satisfy the above criteria, then report the pair of points such that the slope of the line joining the pair is maximum.
(If again, there are multiple pairs of points that satisfy this, then, friend, I don’t know how to help you!)


Report the answer in the form x<sub>i</sub> y<sub>i</sub> x<sub>j</sub> y<sub>j</sub> where x<sub>i</sub> < x<sub>j</sub>. 

In the case where x<sub>i</sub> = x<sub>j</sub>, report your answer in the form x<sub>i</sub> y<sub>i</sub> x<sub>j</sub> y<sub>j</sub> where y<sub>i</sub> < y<sub>j</sub>.  

**Constraints:**

T, N, K, x<sub>i</sub>, y<sub>i</sub>,  are all integers, i ranges from 1 to K.

1<=T<=10000

1<=N<=10000

1<=K<=100000000

0 <= x<sub>i</sub> <= N, 0 <= y<sub>i</sub> <= N


C<sub>i</sub> are strings with values “R”, “G” or “B” corresponding to the colors Red, Green or Blue.


**Input format:**


The first line of input consists of T which is the number of test cases. Then T lines follow with the input as follows:

N K x<sub>1</sub> y<sub>1</sub> x<sub>2</sub> y<sub>2</sub> x<sub>3</sub> y<sub>3</sub> … x<sub>K</sub> y<sub>K</sub> C<sub>1</sub> C<sub>2</sub> … C<sub>K</sub>

The meaning of the input is as follows:

N stands for the maximum value of the x and y co-ordinates of the points.
K is the number of points in the co-ordinate plane that are colored.

C<sub>1</sub>, which is a string of value “R”, “G” or “B” (corresponding to the colors Red, Green or Blue), is the color of the point with coordinates (x<sub>1</sub>, y<sub>1</sub>), C<sub>2</sub> is the color of the point (x<sub>2</sub>, y<sub>2</sub>) and so on up to C<sub>K</sub>.


**Output format:**


x<sub>i</sub> y<sub>i</sub> x<sub>j</sub> y<sub>j</sub>


Where the pair of points (x<sub>i</sub>, y<sub>i</sub>) and (x<sub>j</sub>, y<sub>j</sub>) satisfy the criteria indicated in the question above.


**Sample Input:**

```
1
1 4 0 0 1 0 1 1 0 1 R G R G
```

**Sample Output:**
```
0 0 1 1
```